﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestAPIDemo.MealData;
using RestAPIDemo.MealData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class MealsController : ControllerBase
    {
        private IMealData mealData;

        public MealsController(IMealData mealData)
        {
            this.mealData = mealData;
        }

        [HttpGet]
        [Route("/api/[controller]")]
        public IActionResult GetMeals()
        {
            return Ok(this.mealData.GetMeals());
        }

        [HttpGet]
        [Route("/api/[controller]/{id}")]
        public IActionResult GetMeal(Guid id)
        {
            var meal = this.mealData.GetMeal(id);

            if (meal != null)
            {
                return Ok(this.mealData.GetMeal(id));
            } 
            
            return NotFound($"Meal with Id: {id} not found.");
            
            
        }

        [HttpPost]
        [Route("/api/[controller]")]
        public IActionResult GetMeal(Meal meal)
        {
            this.mealData.AddMeal(meal);
            return Created(HttpContext.Request.Scheme + "://" + HttpContext.Request.Host + HttpContext.Request.Path + "/"
                + meal.Id, meal);
        }

        [HttpDelete]
        [Route("/api/[controller]/{id}")]
        public IActionResult DeleteMeal(Guid id)
        {
            var meal = this.mealData.GetMeal(id);

            if (meal != null)
            {
                this.mealData.DeleteMeal(meal);
                return Ok();
            }
            
            return NotFound($"Meal with Id: {id} not found.");
        }

        [HttpPatch]
        [Route("/api/[controller]/{id}")]
        public IActionResult UpdateMeal(Guid id, Meal updatedMeal)
        {
            var existingMeal = this.mealData.GetMeal(id);

            if (existingMeal != null)
            {
                updatedMeal.Id = existingMeal.Id;
                this.mealData.EditMeal(updatedMeal);
            }

            return Ok(updatedMeal);
        }
    }
}
