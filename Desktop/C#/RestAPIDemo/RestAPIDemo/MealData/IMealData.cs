﻿using RestAPIDemo.MealData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.MealData
{
    public interface IMealData
    {

        List<Meal> GetMeals();

        Meal GetMeal(Guid id);

        Meal AddMeal(Meal meal);

        void DeleteMeal(Meal meal);

        Meal EditMeal(Meal meal);

    }
}
