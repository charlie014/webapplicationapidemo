﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.MealData.Models
{
    public class Meal
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage ="Name can only 50 characters long")]
        public string name { get; set; }

        [Required]
        [MaxLength(150, ErrorMessage = "Description can only be 150 characters long")]
        public string description { get; set; }


    }
}
