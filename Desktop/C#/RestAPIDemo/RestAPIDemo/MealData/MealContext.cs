﻿using Microsoft.EntityFrameworkCore;
using RestAPIDemo.MealData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.MealData
{
    public class MealContext: DbContext
    {
        public MealContext(DbContextOptions<MealContext> options): base(options)
        {

        }

        public DbSet<Meal> Meal { get; set; } 
    }
}
