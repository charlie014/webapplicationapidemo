﻿using RestAPIDemo.MealData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.MealData
{
    public class SqlMealData : IMealData
    {
        private MealContext mealContext;

        public SqlMealData(MealContext mealContext)
        {
            this.mealContext = mealContext;
        }

        public Meal AddMeal(Meal meal)
        {
            meal.Id = Guid.NewGuid();
            this.mealContext.Meal.Add(meal);
            this.mealContext.SaveChanges();
            return meal;
        }

        public void DeleteMeal(Meal meal)
        {
            this.mealContext.Remove(meal);
            this.mealContext.SaveChanges();
        }

        public Meal EditMeal(Meal meal)
        {
            var existingMeal = this.mealContext.Meal.Find(meal.Id);
            if (existingMeal != null)
            {
                existingMeal.name = meal.name;
                this.mealContext.Meal.Update(existingMeal);
                this.mealContext.SaveChanges();
            }

            return meal;
        }

        public Meal GetMeal(Guid id)
        {
            var meal = this.mealContext.Meal.Find(id);
            //return this.mealContext.Meal.SingleOrDefault(x => x.Id == id);
            return meal;
        }

        public List<Meal> GetMeals()
        {
            return this.mealContext.Meal.ToList();
        }
    }
}
