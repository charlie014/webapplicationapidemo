﻿using RestAPIDemo.MealData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.MealData
{
    public class MockMealData : IMealData
    {

        private List<Meal> newMeals = new List<Meal>()
        {
            new Meal()
            {
                Id = Guid.NewGuid(),
                name = "1",
                description = "Description 1"
            },

            new Meal()
            {
                Id = Guid.NewGuid(),
                name = "2",
                description = "Description 2"
            }
        };


        public Meal AddMeal(Meal meal)
        {
            meal.Id = Guid.NewGuid();
            newMeals.Add(meal);

            return meal;
        }

        public void DeleteMeal(Meal meal)
        {
            newMeals.Remove(meal);
        }

        public Meal EditMeal(Meal meal)
        {
            var existingMeal = GetMeal(meal.Id);
            existingMeal.name = meal.name;
            existingMeal.description = meal.description;

            return meal;
        }

        public Meal GetMeal(Guid id)
        {
            return newMeals.SingleOrDefault(x => x.Id == id);
        }

        public List<Meal> GetMeals()
        {
            return newMeals;
        }
    }
}
